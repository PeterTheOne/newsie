## Newsie

[![pipeline status](https://gitlab.com/timrs2998/newsie/badges/master/pipeline.svg)](https://gitlab.com/timrs2998/newsie/commits/master)

An NNTP Client Library targeting NodeJS. It supports the authentication,
TLS encryption, base NNTP commands, and more. 

I started this project after trying several NNTP clients and libraries and being
disappointed in functionality, test coverage, and documentation. This library 
has extensive [tests](./test) pulled directly from the implemented RFC documents
themselves, and several [examples](./examples).

### Usage

Be sure to use the latest LTS (8.9.4+) of NodeJS. Earlier versions of NodeJS are
not supported.

Install the library with yarn or npm:

```bash
npm install newsie
# or
yarn add newsie
```

Use the library to talk to an NNTP server:

```js
const Client = require('newsie').default
const client = new Client({
  host: 'news.easynews.com',
  port: 110
})

client.connect()
  .then(response => {
    console.log(response)
    return client.capabilities()
  })
  .then(response => {
    console.log(response)
    return client.quit()
  })
  .then(response => {
    console.log(response)
  })
```

See the [examples](./examples) folder for more examples.

### Development Notes

In IntelliJ, to debug files in the [src](./src) directory, set `Node Parameters`
to `-r ts-node/register`.

The semantic-release tool requires the [GITLAB_TOKEN](https://semantic-release.gitbooks.io/semantic-release/content/docs/usage/ci-configuration.html#authentication) and [NPM_TOKEN](https://github.com/semantic-release/npm#environment-variables). It is configured with the [@semantic-release/gitlab-config](https://github.com/semantic-release/gitlab-config) package.

### Licensing

All code is licensed under the GNU AFFERO GENERAL PUBLIC LICENSE Version 3.

