import Client, { Options } from '../src/'

const options: Options = {
  // host: 'freenews.netfront.net',
  host: 'freenews.netfront.net',
  responseInterceptor: response => {
    console.log(`[nntp] ${new Date().toISOString()}: ${JSON.stringify(response, undefined, 2)}`)
    return response
  }
}
const client = new Client(options)

const main = async (): Promise<void> => {
  await client.connect()
  // await client.startTls()

  await client.capabilities()
  await client.compressDeflate()

  return client.quit()
}

main().catch(err => {
  console.log('ERROR')
  console.error(err)
  client.disconnect()
})
