import Client from '../src/Client'
import { Server } from '../src/MockServer'

let server
let client
let clientLines
let serverLines
let serverPostAction

function handler(socket, line) {
  expect(line).toEqual(clientLines.join('\r\n') + '\r\n')
  socket.write(serverLines.join('\r\n') + '\r\n')
  serverPostAction()
  return true
}

const s = (line, postAction = () => {}) => {
  serverLines.push(line.replace(/\|/g, '\t'))
  serverPostAction = postAction
}
const c = (...lines: string[]) => {
  serverLines = []
  clientLines = lines
}

function integrationSetup() {
  jest.disableAutomock()

  beforeEach(async () => {
    server = new Server()
    await server.listen()
    client = new Client({
      host: 'localhost',
      port: server.getListeningPort(),
      tlsOptions: {
        rejectUnauthorized: false
      }
    })
    serverPostAction = () => {}
    clientLines = []
    serverLines = []
    server.addHandler(handler)
    await client.connect()
  })

  afterEach(() => {
    client.disconnect()
    return server.close()
  })
}

export { integrationSetup, client, server, s, c }
