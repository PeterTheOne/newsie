export class Socket {
  public on = jest.fn()
  public once = jest.fn()
  public destroy = jest.fn()
  public write = jest.fn((chunk, encodingOrCallback, callback) => {
    callback()
  })
}

export const _socket = new Socket()

export const createConnection = jest.fn(() => _socket)
